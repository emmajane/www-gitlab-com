---
layout: markdown_page
title: "Sales Onboarding"
---

1. [Our Sales Process](https://docs.google.com/document/d/1F0vXw58ctLfk9LKrh35kOSjYvdah4skGGUt46l1-4GM/edit)

1. [Our Sales Agenda](https://docs.google.com/document/d/1l1ecVjKAJY67Zk28CYFiepHAFzvMNu9yDUYVSQmlTmU/edit)

1. [Comparison page on our website](https://about.gitlab.com/comparison/)

1. Our [Basic](https://docs.google.com/a/gitlab.com/document/d/19sjaBytIQeyIiyjXvFpM6QXTDdZNUTO_tQLowlIT_c4/edit), [Standard](https://docs.google.com/document/d/10Ur4nkiyr-qSdgDEVhFkLsoz5RPven8T-1eFmWY26bQ/edit) and Plus Subscription quotes

1. [Our Sales Communication Guide](https://docs.google.com/document/d/1IMDzTj3hZrnsA417z9Ye7WBa8yLkWxGzaLZNJ3O_nVA/edit#heading=h.3nffcmsbeqo7)

1. Login to [Salesforce.com](http://www.salesforce.com/), you should receive an email asking you to change your password:
    * Familiarize yourself with your custom view (https://na34.salesforce.com/00O61000001uYbM) of open opportunities for the month
    * Familiarize yourself with your custom view (https://na34.salesforce.com/00O61000001uYbR) of all open opportunities assigned to you.
    * Familiarize yourself with your custom view (https://na34.salesforce.com/00Q?fcf=00B610000027qT9&rolodexIndex=-1&page=1) of all your open leads.

1. Have your manager grant access to our accounting / finance apps [Recurly](https://app.recurly.com/login). Ask your buddy if they can do a screenshare the next time they process an order.

1. Have your manager grant access to the [Sales Folder](https://drive.google.com/drive/u/0/#shared-with-me) in our Google Docs. In this folder, familiarize yourself with:

1. [Support and development process](/handbook/support-and-development-process)

1. [Giving a GitLab demo](https://about.gitlab.com/handbook/demo/)

1. [Positioning FAQ](https://about.gitlab.com/handbook/positioning-faq)