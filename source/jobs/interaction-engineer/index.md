---
layout: markdown_page
title: "Interaction Engineer Responsibilities and Tasks"
---

## Responsibilities

* Improve screens
* Work with developers to improve flows
* Conduct user testing
* Create wireframes/mockups for new features

## Priorities

1. Work on items tagged with [UI](https://gitlab.com/gitlab-org/gitlab-ce/issues?label_name=ui) and/or [UX](https://gitlab.com/gitlab-org/gitlab-ce/issues?label_name=ux) and assigned to a milestone
1. Work on items under the interaction header https://about.gitlab.com/direction/
1. Work on items tagged with [UI](https://gitlab.com/gitlab-org/gitlab-ce/issues?label_name=ui) and/or [UX](https://gitlab.com/gitlab-org/gitlab-ce/issues?label_name=ux) and not assigned to a milestone


## Success Criteria
You know you are doing a good job as an Interaction Engineer when:
* you are resolving UX / UI issues assigned to milestones well before the milestone comes up
* you communicate well with the developers
* you are contributing ideas and solutions beyond existing issues
* users are overwhelmingly happy about your contributions